<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithDrawRequests extends Model
{
    protected $table = 'withdrawing_requests';

    public function get($id = null)
    {
        if ($id) {
            return self::where("user_id", $id)->get();
        } else {
            return self::all();
        }
    }
}
