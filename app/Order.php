<?php

namespace App;

use App\Constant;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;


use App\Http\Controllers\OrderController;


class Order extends Model
{
    //
    protected $table = "orders";

    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }

    public function GetByType($type)
    {
        $userId = Auth::user()->id;
        return self::where('service_id', $type)->where('user_id', $userId)->orderBy('created_at', 'DESC')->get();
    }

    public function GetByTypeView($type)
    {
        return self::where('type', $type)->get();
    }

    public static function GetRemainingOrders($type)
    {
        return DB::table('orders')
            ->join('services', "orders.service_id", "=", "services.id")
            ->where('orders.status', Constant::STATUS_ORDER_INCOMPLETE)
            ->where('services.type', $type)->count();
    }

    public function type()
    {
        return $this->belongsTo('App\Service', "service_id");
    }

    public function user()
    {
        return $this->belongsTo('App\User', "user_id");
    }

    public function CountOrderByUserId($user_id, $status)
    {
        return self::where('status', $status)->where('user_id', $user_id)->count();
    }

    public function Create($service_id, $target, $total, $quantity, $note, $duration = null, $moreConfigs = [])
    {
        $newOrder = new Order();
        $service = Service::find($service_id);
        $newOrder->user_id = Auth::user()->id;
        $newOrder->service_id = $service->id;
        $newOrder->target = $target;
        $newOrder->total = $total;
        $newOrder->quantity = $quantity;
        if (isset($moreConfigs['api_id'])) {
            $newOrder->api_id = $moreConfigs['api_id'];
        }

        if (isset($moreConfigs['profit'])) {
            $newOrder->profit = $moreConfigs['profit'];
        }

        if (isset($moreConfigs['result']->order_id)) {
            $orderId = $moreConfigs['result']->order_id;

            if(isset($orderId)) {
                $newOrder->api_id = $orderId;
            }
        }

        if ($duration) {
            $newOrder->duration = $duration;
        } else {
            $newOrder->duration = $service->unit_duration * $newOrder->quantity;
        }

        $newOrder->note = $note;
        $newOrder->save();
        $transaction = new Transaction;
        $transaction->Create($newOrder->user_id, 1, -$newOrder->total, $service->name);
        $user = Auth::user();
        $user->balance -= $newOrder->total;
        $user->save();

        return $newOrder;
    }

    public function CheckCompleted($orderList)
    {

        foreach ($orderList as $order) {

            if ($order->status == Constant::STATUS_ORDER_COMPLETED) {
                continue;
            }
            if ($order->status == Constant::STATUS_ORDER_ERROR) {
                continue;
            }

            if (strtotime($order->created_at) + $order->duration <= strtotime("now")) {
                $order->status = Constant::STATUS_ORDER_COMPLETED;
                $order->save();
            }
        }
    }

    public static function CheckDuplicate($target_id, $service_id, $userId)
    {
        $duplicateOrderCount = DB::table('orders')
            ->join('services', "orders.service_id", "=", "services.id")
            ->where('services.id', '=', (int)$service_id)
            ->where('orders.target', '=', $target_id)
            ->where('services.block_duplicate', '=', 1)
            ->where('orders.user_id', '=', $userId)
            ->where(function ($query) {
                $query->where('orders.status', '=', Constant::STATUS_ORDER_INCOMPLETE)
                    ->orWhere('orders.status', '=', Constant::STATUS_ORDER_INCOMPLETED_2);
            });
        return $duplicateOrderCount->count() > 0 ? true : false;
    }

    public function UpdateData()
    {
    }
}
