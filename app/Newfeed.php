<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newfeed extends Model
{
    protected $table = "newfeeds";

    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }
}





