<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Service extends Model
{
    //
    protected $table = "services";

    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }
    public function GetByTypeView($type)
    {
        return self::where('type', $type)->get();
    }

    public static function GetPrice($service_id, $role)
    {
        $service = self::find($service_id);
        switch ($role) {
            case 0:
            {
                return $service->apiPrice;
            }
            case 1:
            {
                return $service->agencyPrice;
            }
            case 2:
            {
                return $service->collaboratorsPrice;
            }
            case 3:
            {
                return $service->guestPrice;
            }
        }
    }

    public static function GetPriceMap()
    {
        $services = self::all();
        $user = Auth::user();
        $priceKey = "agencyPrice";
        switch ($user->role) {
            case 0:
            {
                $priceKey = "apiPrice";
                break;
            }
            case 1:
            {
                $priceKey = "agencyPrice";
                break;
            }
            case 2:
            {
                $priceKey = "collaboratorsPrice";
                break;
            }
            case 3:
            {
                $priceKey = "guestPrice";
                break;
            }
        }
        $priceMap = [];
        foreach ($services as $service) {
            $priceMap[$service['slug']] = $service[$priceKey];
            $priceMap[$service['id']] = $service[$priceKey];
        }
        return $priceMap;
    }

    public static function ShouldBlockDuplicate($service_id)
    {
        $service = self::find($service_id);
        return $service->block_duplicate == 1 ? true : false;
    }


    public function category()
    {
        return $this->belongsTo('App\Category', "category_id");
    }
}
