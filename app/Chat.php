<?php

namespace App;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Chat extends Model
{
    protected $table = "chats";

    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }

    //    public function getUserMessages($userId)
    //    {
    //        $messages = DB::select(DB::raw("SELECT myChats2.*, users.name as from_name, users.id as from_id FROM (
    //			SELECT myChats.*, users.name as target_name, myChats.user_id as from_id FROM (
    //                SELECT * FROM `chats` WHERE target_id = $userId  OR user_id = $userId
    //            ) AS myChats
    //				INNER JOIN users ON users.id = myChats.target_id) as myChats2
    //INNER JOIN users ON myChats2.from_id = users.id   ORDER BY myChats2.created_at DESC"));
    //        return $messages;
    //    }

    public function getMessages($fromId, $targetId)
    {
        $messages = DB::select(DB::raw("SELECT * FROM `chats` WHERE (target_id = $fromId AND user_id = $targetId) OR (user_id = $fromId AND target_id = $targetId)"));
        return $messages;
    }

    public function getSenders($fromUserId)
    {
        $senders = DB::select(DB::raw("SELECT chats.*, users.id as from_id, users.name as from_name FROM chats INNER JOIN users ON users.id = chats.user_id WHERE target_id = $fromUserId ORDER BY created_at ASC"));
        return $senders;
    }

    public static function getUnreadMessage()
    {
        $user = Auth::user();
        $unreadMessages = self::where('seen', 0)->where('target_id', $user->id)->get();
        return $unreadMessages;
    }


}
