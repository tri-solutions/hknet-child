<?php

namespace App\Http\Middleware;

use App\Order;
use Illuminate\Support\Facades\Auth;

use Closure;

use App\Service;

class CheckBalance
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$data = [
			"service_id" => $request->input("service_id"),
			"amount" => $request->input("amount"),
		];

		$user = Auth::user();
		$total = Service::GetPrice($data["service_id"], $user->role) * $data["amount"];

		if ($total > $user->balance) {
			return response()->json(["message" => "Số dư không đủ !!"]);
		}

		// Check Duplicate
        $should_block_duplicate = Order::CheckDuplicate($request->input('id'), $data['service_id'], $user->id);
		if($should_block_duplicate) {
            return response()->json(["message" => "Các đơn đang xử lý & đang chạy sẽ không order mới được cho tới khi hoàn tất !", "success" => 0]);
        }

		return $next($request);
	}
}
