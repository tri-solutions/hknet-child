<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
	/**
	 * The URIs that should be excluded from CSRF verification.
	 *
	 * @var array
	 */
	protected $except = [
	    //facebok
		"api/likepost",
		"api/likepost2",
		"api/subnhanh",
		"api/viewfacebook",
		"api/buffmember",
		"api/likepagegiare",
		"api/subthuc",
		"api/likepagethuc",
		"api/buffcomment",
		"api/buffcomment2",
		"api/buffreviewpage",
		"api/bufflive",
		"api/bufflive2",
		"api/viplike",
		"api/vipcomment",
		"api/sharewall",
		"api/sharelive",
		"api/tangbanbe",
		//intsagram
		"api/viewinstagram",
		"api/likeinstagram",
		"api/subinstagram",
		//tiktok
		"api/subtiktok",
		"api/liketiktok",
		"api/viewtiktok",
		"api/commenttiktok",
		"api/sharetiktok",
		//youtube
		"api/subyoutube",
		"api/likeyoutube",
		"api/commentyoutube",
		"api/viewyoutube",
		"api/gioxemyoutube",
        // More
        "admin/mail/sendOtp",
        // Public
        "/api/webhook/order"
	];
}
