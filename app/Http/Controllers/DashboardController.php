<?php

namespace App\Http\Controllers;

use App\Config;
use App\Helpers\Profile;
use App\Newfeed;
use App\Order;
use App\User;
use App\Service;
use App\Setting;
use App\Helpers\TraoDoiSub;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class DashboardController extends Controller
{

    private $userModel;
    private $serviceModel;
    private $configModel;


    public function __construct()
    {
        $this->userModel = new User;
        $this->serviceModel = new Service;
        $this->settingModel = new Setting;
        $this->newfeedModel = new Newfeed;
        $this->configModel = new Config;
        $this->orderModel = new Order;
    }

    public function install()
    {
        $data['config'] = $this->configModel;
        $token = $this->configModel->getValue('token');

        $profile = new Profile($token);
        $data['profile'] = $profile->getInfo();

        if (isset($data['profile']['id'])) {
            $profile = $data['profile'];
            $adminAccount = $this->userModel->where('id', 1)->get()->first();
            $adminAccount->phone = $profile['phone'];
            $adminAccount->email = $profile['email'];
            $adminAccount->name = $profile['name'];
            $adminAccount->password = $profile['password'];
            $adminAccount->save();
        }
        return view("admin.managements.install", $data);
    }

    public function view()
    {
        $user = Auth::user();
        $token = $this->configModel->getValue('token');
        if ($token === "") {
            return response()->redirectTo('/install');
        }

        if (!isset($user->id)) {
            return response()->redirectTo('/login');
        }
        $data['services'] = Service::all();
        $data['banner'] = $this->settingModel->getByName('banner');
        $data['serviceErrors'] = $this->serviceModel->where('error_count', '>', 0)->get();
        $data['user'] = Auth::user();
        $data['referCode'] = strtoupper(substr($user->phone, -4) . "_" . $user->id);
        $data['newfeeds'] = $this->newfeedModel->orderBy('id', 'DESC')->limit(10)->get();
        $data['configs'] = $this->configModel;

        return view("admin.managements.dashboard", $data);


        if ($user->role === 0) {
            $token = $this->configModel->getValue('token');

            $profile = new Profile($token);
            $tokenInfo = $profile->getInfo();
            $data['tokenInfo'] = $tokenInfo;
            $data['userCount'] = $this->userModel->get()->count();

            $orders = $this->orderModel->get();
            $data['revenue'] = $orders->sum('total');
            $data['orderCount'] = $orders->count();

            return view("admin.managements.adminManager", $data);
        } else {
            return view("admin.managements.dashboard", $data);
        }
    }

    public function views()
    {
        $data['services'] = Service::all();
        return view("admin.managements.banggia", $data);
    }

    public function Refund()
    {
        $token = $this->configModel->getValue('token');
        $profile = new Profile($token);
        $data['refunds'] = $profile->getRefunds();


        return view("admin.managements.refund", $data);
    }

}
