<?php

namespace App\Http\Controllers;

use App\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    private $model;

    public function __construct()
    {
        $this->model = new Chat;
    }

    public function index() {

        $data['user'] = Auth::user();
        return view("admin.features.chat", $data);
    }

    public function sendMessage(Request $request) {
        $user = Auth::user();
        $message = $request->input('messsage');
        $target = $request->input('target');


        if(!isset($message) ||$message === "") {
            return response()->json([
                "status" => 0,
                "message" => "Vui lòng điên nội dung cần gửi"
            ], 402);
        }

        $chat  = new Chat();
        $chat->message = strip_tags($message);
        $chat->user_id = $user->id;
        $chat->target_id = $target;
        $chat->created_at = date('Y-m-d H:i:s');
        $chat->save();
        return response()->json([
            "status" => 1,
        ]);
    }

    public function getUserMessages(Request $request) {
        $user = Auth::user();
        $messages = $this->model->getUserMessages($user->id);
        return response()->json($messages);
    }

    public function getSenders(Request $request) {
        $user = Auth::user();
        $senders = $this->model->getSenders($user->id);
        return response()->json($senders);
    }


    public function getMessages(Request $request) {
        $targetUser = Auth::user();
        $fromUser = $request->input('from');
        $messages = $this->model->getMessages($fromUser, $targetUser->id);
        return response()->json($messages);
    }


    public function getUnReadMessage(Request $request) {
        $targetUser = Auth::user();
        $unReadMessages = $this->model->where('target_id', $targetUser->id)->where('seen', 0)->orderBy('created_at', 'DESC')->get();
        return response()->json($unReadMessages);
    }

    public function setSeen(Request $request) {
        $targetSeen = $request->input('user_id');
        $user = Auth::user();
        $this->model->where('user_id', $targetSeen)->where('target_id', $user->id)->update([
            'seen' => 1
        ]);
        return response()->json([
            "status" => 1
        ]);
    }


}
