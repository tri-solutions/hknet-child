<?php

namespace App\Http\Controllers;

use App\Config;
use App\Helpers\Profile;
use App\Order;
use App\User;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    public $configModel;

    public function __construct()
    {
        $this->configModel = new Config;
        $this->userModel = new User;
        $this->orderModel = new Order;
    }

    public function index()
    {
        $data['config'] = $this->configModel;
        $token = $this->configModel->getValue('token');

        $profile = new Profile($token);
        $tokenInfo = $profile->getInfo();
        $data['tokenInfo'] = $tokenInfo;
        $data['banks'] = $this->configModel->getLike("bank_");

        $data['userCount'] = $this->userModel->get()->count();

        $orders = $this->orderModel->get();
        $data['profit'] = $orders->sum('profit');
        $data['orderCount'] = $orders->count();

        return view("admin.managements.config", $data);
    }

    public function delete(Request $request)
    {
        $configKey = $request->input("configKey");

        $result = $this->configModel->where("config_key", $configKey)->delete();

        if ($result) {
            return response()->json([
                "status" => true,
                "message" => "Deleted"
            ]);
        } else {
            return response()->json([
                "status" => false,
                "message" => "Fail"
            ]);
        }
    }

    public function save(Request $request)
    {
        $configs = $request->input();
        foreach ($configs as $name => $value) {
            if ($name === 'token') {
                $profile = new Profile($value);
                $tokenInfo = $profile->getInfo();
                if (isset($tokenInfo['error']) || (isset($tokenInfo['success']) && $tokenInfo['success'] === 0) ) {
                    return response()->json([
                        "status" => false,
                        "message" => $tokenInfo['message']
                    ], 400);
                }
            }
            $this->configModel->setConfig($name, $value);
        }
        return response()->json([
            "status" => true,
            "message" => "Cập nhật thành công"
        ]);
    }

    public function saveBank(Request $request)
    {
        $bankName = $request->input("bank");
        $configName = "bank_${bankName}";
        $bankData = $request->input();

        $this->configModel->setConfig($configName, json_encode($bankData));

        return response()->json([
            "status" => true,
            "message" => "Thêm bank thành công"
        ]);
    }

    public function applyTheme(Request $request)
    {
        $_CONFIG_NAMES = [
            "LANDING_PAGE" => "landing_theme",
            "LOGIN_PAGE" => "login_theme",
        ];

        $themeId = $request->input('theme');
        $type = $request->input('type');

        $configName = $_CONFIG_NAMES[$type];
        $result = $this->configModel->setConfig($configName, $themeId);

        if ($result) {
            return response()->json([
                "status" => true,
                "message" => "Cập nhật thành công"
            ]);
        } else {
            return response()->json([
                "status" => false,
                "message" => "Xảy ra lỗi khi cập nhật"
            ]);
        }
    }
}
