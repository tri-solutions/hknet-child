<?php

namespace App\Http\Controllers;

use App\IpLog;
use App\GiftCode;
use App\Newfeed;
use App\Partner;
use App\Setting;

use App\Token;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{

    private $model;
    private $transactionModel;
    private $partnerModel;
    private $giftCode;
    private $tokenModel;


    public function __construct()
    {
        $this->model = new Setting;
        $this->partnerModel = new Partner;
        $this->giftCode = new GiftCode;
        $this->tokenModel = new Token;
        $this->newfeed = new Newfeed;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['settings'] = $this->model->get();
        $data['giftCodes'] = $this->giftCode->get();
        $data['partners'] = $this->partnerModel->get();
        $data['newfeeds'] = $this->newfeed->get();
        $data['tokens'] = DB::table("tokens")->where('isExpired', 0)->get()->toArray();
        return view("admin.managements.setting", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param IpLog $setting
     * @return Response
     */
    public function update(Request $request, IpLog $setting)
    {
        $name = $request->input('name');
        $value = $request->input('value');
        $setting = Setting::where('name', $name)->first();
        $setting->name = $name;
        $setting->value = $value;
        $setting->save();
        return response([
            "name" => $name,
            "values" => $value
        ]);
    }


    public function saveNewfeed(Request $request)
    {
        $result = DB::table("newfeeds")->insert($request->input());
        var_dump($result);
    }

    public function deleteNewfeed(Request $request)
    {
        $id = $request->input('id');
       $result =  $this->newfeed->where('id', $id)->delete();
       return response()->json($result);
    }

    public function addToken(Request $request)
    {
        $tokens = $request->input("tokens");
        $result = DB::table("tokens")->insert($tokens);
        return response($result);
    }

    public function createGifCode(Request $request)
    {
        $code = $request->input("code");
        $quantity = $request->input("quantity");
        $value = $request->input("value");

        $existedGifCode = $this->giftCode->where('code', $code)->get();
        if (isset($existedGifCode->id)) {
            return response([
                "success" => 0,
                "values" => "Code existed"
            ], 301);
        } else {
            $giftCode = new GiftCode();
            $giftCode->code = $code;
            $giftCode->quantity = $quantity;
            $giftCode->value = $value;
            $giftCode->used = 0;
            $giftCode->save();
            return response([
                "success" => 1,
            ]);
        }
    }

    public function deleteGiftCode(Request $request)
    {
        $id = $request->input("id");
        $giftCode = $this->giftCode->get($id);
        $giftCode->delete();
        echo "ok";
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param IpLog $setting
     * @return Response
     */
    public function destroy(IpLog $setting)
    {
        //
    }
}
