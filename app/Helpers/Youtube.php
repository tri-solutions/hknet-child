<?php

namespace App\Helpers;

class Youtube extends HkNet
{
    public function __construct($token = null)
    {
        parent::__construct($token);
    }

    public function buffSub($id, $amount)
    {
        return $this->post('/subyoutube', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffLike($id, $amount)
    {
        return $this->post('/likeyoutube', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffView($id, $amount)
    {
        return $this->post('/viewyoutube', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffDislike($id, $amount)
    {
        return $this->post('/buffDislikeYoutube', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function worldView($id, $amount)
    {
        return $this->post('/worldview', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffComment($id, $noidung)
    {
        return $this->post('/commentyoutube', [
            "id" => $id,
            "noidung" => $noidung,
        ]);
    }

    public function buffTimeView($id, $amount)
    {
        return $this->post('/gioxemyoutube', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }


}
