<?php

namespace App\Helpers;

class Telegram extends HkNet
{
    public function __construct($token = null)
    {
        parent::__construct($token);
    }

    public function buffMember($id, $amount)
    {
        return $this->post('/memberTelegram', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }
}
