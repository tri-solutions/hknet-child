$(function () {
    "use strict";

    $(".order-info [name=comment]").on("change", function () {
        const comments = $(this).val();
        const lines = $(".order-info [name=comment]").val().lineCount();
        const price = $(this).data("price");
        $(".order-info [name=total]").val(price * lines);
    });

    $("#orderBtn").on("click", function () {
        const button = $(this);
        button.attr("disabled", true);
        const buffType = button.data("function");
        const backupBtn = button.html();
        const loadingBtn =
            '<i class="fa fa-spin fa-circle-o-notch"></i> Thực hiện';
        button.html(loadingBtn);

        // Check comment lines
        const lines = $(".order-info [name=comment]").val().lineCount();
        const comments = $(".order-info [name=comment]").val().trim().split('\n');
        const uniqueComments = [...new Set(comments)];

        if (comments.length > uniqueComments) {
            swal(
                {
                    title: "Lỗi!",
                    text: ` Comments ko được trùng nhau !!!`,
                    type: "error",
                },
                function () {}
            );
            button.attr("disabled", false);
            button.html(backupBtn);
            return;
        }

        if (lines < 5) {
            swal(
                {
                    title: "Lỗi!",
                    text: `Ít nhất 5 comments !`,
                    type: "error",
                },
                function () {}
            );
            button.attr("disabled", false);
            button.html(backupBtn);
            return;
        }

        let data = {};

        data.service_id = button.data("service");

        data.id = $(".profile-info [name=id]").val();

        data.comment = $(".order-info [name=comment]").val();
        data.note = $(".order-info [name=note]").val();
        data.gender = $(".order-info [name=gender]").val();

        data.amount = $(".order-info [name=comment]").val().lineCount();

        $.ajax({
            url: `/admin/dichvu/facebook-buff/${buffType}`,
            method: "POST",
            dataType: "json",
            data,
        }).always(function (data) {
            if (data) {
                if (data.success == 1) {
                    swal(
                        {
                            title: "Thành công!",
                            text: `Thêm yêu cầu thành công !`,
                            type: "success",
                        },
                        function () {
                            location.reload(true);
                        }
                    );
                } else {
                    button.attr("disabled", false);
                    button.html(backupBtn);
                    swal(
                        {
                            title: "Failed!",
                            text: data.message,
                            type: "error",
                        },
                        function () {
                            button.attr("disabled", false);
                        }
                    );
                }
            }
            button.html(backupBtn);
        });
    });
});

String.prototype.lines = function () {
    return this.split(/\r*\n/);
};
String.prototype.lineCount = function () {
    return this.lines().length;
};
