$(function () {
    "use strict";
    $(".order-info [name=comment]").on("change", function () {
        const comments = $(this).val();
        const lines = $(".order-info [name=comment]").val().lineCount();
        const price = $(this).data("price");
        const amount = 125;
        $(".order-info [name=total]").val(price * lines * amount);
    });
    $("#orderBtn").on("click", function () {
        const button = $(this);
        button.attr("disabled", true);
        const buffType = button.data("function");
        const backupBtn = button.html();
        const loadingBtn =
            '<i class="fa fa-spin fa-circle-o-notch"></i> Thực hiện';
        button.html(loadingBtn);

        const form = $("#infoForm");

        let formData = form.serializeArray();
        let data = {};
        $(formData).each(function (index, obj) {
            data[obj.name] = obj.value;
        });

        data.id = $("#profileLink").val();

        data.service_id = button.data("service");

        $.ajax({
            url: `/admin/dichvu/other/${buffType}`,
            method: "POST",
            dataType: "json",
            data,
        }).always(function (data) {
            if (data.success == 1) {
                swal(
                    {
                        title: "Thành công!",
                        text: `Thêm yêu cầu thành công !`,
                        type: "success",
                    },
                    function () {
                        location.reload(true);
                    }
                );
            } else {
                button.attr("disabled", false);
                button.html(backupBtn);
                swal(
                    {
                        title: "Failed!",
                        text: data.message,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            }
            button.html(backupBtn);
        });
    });
});

String.prototype.lines = function () {
    return this.split(/\r*\n/);
};
String.prototype.lineCount = function () {
    return this.lines().length;
};