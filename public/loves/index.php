
<html lang="en">
	
<!-- Mirrored from demo.regediter.com/arlin-landing/arlin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 May 2018 09:57:00 GMT -->
<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Arlin - Responsive HTML5 Wedding Template">
		<meta name="author" content="regediter.com">
			
		<!-- Page Title -->
		<title>Tặng Em 20-10... Ty Võ</title>

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Favicon -->
		<link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon">
		<link href="img/apple-touch-icon.png" rel="apple-touch-icon">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/owlcarousel/css/owl.carousel.min.css">
		<link rel="stylesheet" href="vendor/owlcarousel/css/owl.theme.default.min.css">
		<link rel="stylesheet" href="vendor/animate_css/animate.css">
		<link rel="stylesheet" href="vendor/magnific-popup/css/magnific-popup.css">
		<link rel="stylesheet" href="vendor/vegas/dist/vegas.min.css">

		<!-- Fonts CSS -->
		<link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Raleway:200,300,400,500" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/style.css?sssss">

	</head>
	<body>
		<!-- Page Loader Animation -->
		<div id="loader">
			<div class="container loader-img-holder text-center">
				<img class="img-fluid" src="img/loader.svg" alt="">
			</div>
		</div>		
		<!-- Page Loader Animation End-->

		<!-- slider Section -->
		<section id="slider">
			<div class="holder-caption">
				<div class="container relative-z">
					<div class="row">
						<div class="col col-lg-12 align-self-center">
							<h1>Yêu em...<span class="custom-color">Ty Võ !!</span></h1>
							<div class="slider-text-holder">
								<h2>20-10</h2>
								<h3>Tặng em một món quà nhỏ bé đầy ắp sự chân thành này, Anh chúc Ty Em những điều tốt đẹp nhất trên đời. Anh mong em sẽ là người bạn đồng hành của anh hôm nay, ngày mai và mãi mãi về sau. Anh yêu em nhiều !Chúc em 20/10 thật vui vẻ ^^!</h3>

Ngày đặc biệt chỉ còn

								<div class="slider-img-holder"></div>
							</div>
							<div class="justify-content-lg-center counter">
								<div id="countdown"></div>
							</div>

						</div>
					</div>
				</div>
				<div class="slider-scroll">
					<a class="scroll-link" href="#about">
						<i class="x">Xem tiếp</i>
					</a>
				</div>
				<div class="img-overlay"></div>
			</div>
		</section>
		<!-- slider Section End -->


		<!-- About Section -->
		<section id="about">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="big-head-wrap">
							<img class="headline-hearth" src="img/headline_hearth.svg" alt="wedding-head-image">
						</div>
	<h1><span class="custom-color align-self-center">Yêu Em</span></h1>
						<h2 class="big-heading mt-0 mt-0"><span class="custom-color"><date>0 Ngày</date> Ngày <time></time> Giây</span></h2>
								<h4><div id="heart"><anni>...</anni></div></h4>
						<img class="headline_1" src="img/headline_simple.svg" alt="">


<script language="javascript">
cusor='+';
count=10;
mess='Ty... em biết gì không<br />'+
'-------------------------- <br />'+
'Thế giới anh thay đổi từ khi gặp em.. <br />'+
'Dường như chưa từng có ai hiểu anh cho đến khi em biết đến anh <br>'+
'Dường như chẳng có ai yêu quý anh cho đến khi em yêu anh <br>'+
'Dường như chẳng có ai muốn chạm vào anh cho đến khi em đến bên anh <br>'+
'Chẳng ai cả.. cho đến khi em xuất hiện <br>'+
'Chỉ một cái chạm với em đã đủ khiến anh say đắm ^^<br>'+
'Cảm giác anh và em gặp nhau là một định mệnh <br>'+
'Mọi thứ thật tuyệt khi anh gặp em<br /> '+
'Chúng ta hợp từ tính cách đến những cử chỉ những hành động...<br />'+
'Và cả cảm xúc của nhau... <br />'+
'Mọi thứ thật hoàn hảo<br />'+
'Từ khi bắt đầu cho đến khoảnh khắc này.<br/ >'+
'Anh chỉ muốn nói rằng !<br/ >'+
'Cảm ơn em vì em đã đến bên anh và ở lại trong trái tim này.<br/ >'+
'Yêu Em... Ty Võ !!<br/ >'
 
function type()
{
if (cusor=='+') cusor=' '; else cusor='+';
document.getElementById('scrs').innerHTML=mess.substring(0,count++)+cusor;
if(count<=mess.length) setTimeout("type()",200);
}
</script>
<body onload="type()">
<div id="scrs"></div>
</body>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 text-center margin-xs-b-2 margin-sm-b-2">
						<figure>
							<img src="img/tyvor.jpg" alt="bride" class="img-fluid rounded-circle">
						</figure>
						<div class="about-content col-sm-8 offset-sm-2">
							<h4>Ty Võ
								<span class="custom-color"></span>
							</h4>
							<p>Em là bản remix trộn lẫn tồ lô trên hoàn hảo</p>

						</div>
					</div>
					<div class="col-lg-6 text-center">
						<figure>
							<img src="img/huuky.jpg" alt="groom" class="img-fluid rounded-circle">
						</figure>
						<div class="about-content col-sm-8 offset-sm-2">
							<h4>Hữu Ký
								<span class="custom-color"></span>
							</h4>
							<p>Anh là bản trữ tình đưa em vào quỹ đạo :v</p>

						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- About Section End -->



		<!-- Story Section
		<section id="story">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="big-head-wrap">
							<img class="headline-hearth" src="img/headline_hearth.svg" alt="">
						</div>
						<h2 class="big-heading mt-0 mt-0">
							<span class="custom-color">Em phải chăng là</span> Nàng Thơ ?
						</h2>
						<img class="headline_1" src="img/headline_simple.svg" alt="">
						<p class="sub-heading col-lg-8 offset-lg-2 margin-b-5">Em là nguồn cảm hứng khiến anh bay bỗng vô tận, chính đều đó một số bài thơ câu hát vượt ra ngoài khuôn khổ của sự khô cứng.</p>
					</div>
				</div>

				<div class="timeline">
					<div class="row">
						<div class="col-sm-12 col-md-6 pr-70 text-center">
							<div class="tline-thumbnail story-img-1">
								<div class="img-date-left">
									<div class="day">12</div>
									<div class="month">jan</div>
								</div>
							</div>
						</div>
						<div class="tline-right-side col-sm-12 col-md-6 pl-70 text-left date-circle margin-b-2">
							<div class="tline-arrow"></div>							
							<h4 class="tline-title font-weight-light">Nắng Hạ</h4>
							<p>Dạo cùng anh, với buổi chiều nắng hạ<br>
							Em thẹn thùng, tựa đầu vào vai anh<br>
							Vẫn câu hỏi, hôm nay đi đâu thế<br>
							Ngọn Hải Đăng, Vịnh Bán Đảo Sơn Trà<br>
							Nếu em thích, Cùng Anh Đi Dạo Nhé.</p>

						</div>
					</div>
					<div class="row">
						<div class="col-md-6 pl-70 text-center order-2 main-order-xs-1 main-order-sm-1">
							<div class="tline-thumbnail story-img-2">
								<div class="img-date-right">
									<div class="day">12</div>
									<div class="month">jan</div>
								</div>
							</div>
						</div>
						<div class="tline-left-side pr-70 col-md-6 text-right date-circle order-1 main-order-xs-2 main-order-sm-2 margin-b-2">
							<div class="tline-arrow"></div>
							<h4 class="tline-title font-weight-light">First date</h4>
							<p>Donec a velit molestie, auctor elit a, varius nibh. Nunc hendrerit, leo eu interdum auctor, nibh massa auctor mi, id semper ante nibh egestas ex. Praesent ex dolor.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-6 pr-70 text-center">
							<div class="tline-thumbnail story-img-3">
								<div class="img-date-left">
									<div class="day">12</div>
									<div class="month">jan</div>
								</div>
							</div>
						</div>
						<div class="tline-right-side col-md-6 pl-70 text-left date-circle margin-b-2">
							<div class="tline-arrow"></div>
							<h4 class="tline-title font-weight-light">The Proposal</h4>
							<p>Donec a velit molestie, auctor elit a, varius nibh. Nunc hendrerit, leo eu interdum auctor, nibh massa auctor mi, id semper ante nibh egestas ex. Praesent ex dolor.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-6 pl-70 text-center order-2 main-order-xs-1 main-order-sm-1">
							<div class="tline-thumbnail story-img-4">
								<div class="img-date-right">
									<div class="day">12</div>
									<div class="month">jan</div>
								</div>
							</div>
						</div>
						<div class="tline-left-side col-md-6 pr-70 text-right date-circle order-1 main-order-xs-2 main-order-sm-2 margin-b-2">
							<div class="tline-arrow"></div>
							<h4 class="tline-title font-weight-light">Our wedding</h4>
							<p>Donec a velit molestie, auctor elit a, varius nibh. Nunc hendrerit, leo eu interdum auctor, nibh massa auctor mi, id semper ante nibh egestas ex. Praesent ex dolor.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		 -->
		<!-- Comment Section -->
		<section id="comment">
			<div class="container relative-z">
				<div class="row">
					<div class="col-lg-10 offset-lg-1">
						<div id="comment-carousel" class="owl-carousel owl-theme">
							<div class="item">
								<h3 class="heading text-center">Em là nguồn cảm hứng khiến anh bay bỗng vô tận, chính đều đó một số bài thơ câu hát vượt ra ngoài khuôn khổ của sự khô cứng..</h3>
							</div>
							<div class="item">
								<h3 class="heading text-center">Em là nguồn cảm hứng khiến anh bay bỗng vô tận, chính đều đó một số bài thơ câu hát vượt ra ngoài khuôn khổ của sự khô cứng..</h3>
							</div>
							<div class="item">
								<h3 class="heading text-center">Em là nguồn cảm hứng khiến anh bay bỗng vô tận, chính đều đó một số bài thơ câu hát vượt ra ngoài khuôn khổ của sự khô cứng..</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="img-overlay"></div>
		</section>
		<!-- Comment Section End -->
		<!-- Portfolio Section -->
		<section id="portfolio">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="big-head-wrap">
							<img class="headline-hearth" src="img/headline_hearth.svg" alt="">
						</div>
						<h2 class="big-heading mt-0 mt-0">
							<span class="custom-color">Album</span> Love
						</h2>
						<img class="headline_1" src="img/headline_simple.svg" alt="">
					</div>
				</div>
			</div>

			<!-- Portfolio Nav-->
			<div class="container-fluid">
				<div class="no-margin">
					<div class="button-group filters-button-group margin-b-4">
						<button class="button is-checked" data-filter="*">All</button>
						<button class="button" data-filter=".wedding">Đi chơi</button>
						<button class="button" data-filter=".party">Ăn uống</button>
						<button class="button" data-filter=".videos">Videos</button>
					</div>
				</div>
				<!-- Portfolio Nav End -->

				<!-- Portfolio Images -->
				<div class="row grid">
					<div class="grid-item popup-gallery col-lg-3 col-md-6 col-sm-12 party transition" data-category="transition">
						<div class="item">
							<a href="img/portfolio_1.jpg">
								<div class="thumbnail-overlay">
									<img class="img-fluid" src="img/portfolio_1.jpg" alt="">
									<span class="thumbnail-title">
										<i class="fa fa-search-plus"></i>
									</span>
								</div>
							</a>
						</div>
					</div>
					<div class="grid-item popup-gallery col-lg-3 col-md-6 col-sm-12 wedding transition" data-category="transition">
						<div class="item">
							<a href="img/portfolio_2.jpg">
								<div class="thumbnail-overlay">
									<img class="img-fluid" src="img/portfolio_2.jpg" alt="">
									<span class="thumbnail-title">
										<i class="fa fa-search-plus"></i>
									</span>
								</div>
							</a>
						</div>
					</div>
					<div class="grid-item popup-gallery col-lg-3 col-md-6 col-sm-12 party transition" data-category="transition">
						<div class="item">
							<a href="img/portfolio_3.jpg">
								<div class="thumbnail-overlay">
									<img class="img-fluid" src="img/portfolio_3.jpg" alt="">
									<span class="thumbnail-title">
										<i class="fa fa-search-plus"></i>
									</span>
								</div>
							</a>
						</div>
					</div>
					<div class="grid-item popup-gallery col-lg-3 col-md-6 col-sm-12 wedding transition" data-category="transition">
						<div class="item">
							<a href="img/portfolio_4.jpg">
								<div class="thumbnail-overlay">
									<img class="img-fluid" src="img/portfolio_4.jpg" alt="">
									<span class="thumbnail-title">
										<i class="fa fa-search-plus"></i>
									</span>
								</div>
							</a>
						</div>
					</div>
					<div class="grid-item popup-gallery col-lg-3 col-md-6 col-sm-12 wedding transition" data-category="transition">
						<div class="item">
							<a href="img/portfolio_16.jpg">
								<div class="thumbnail-overlay">
									<img class="img-fluid" src="img/portfolio_5.jpg" alt="">
									<span class="thumbnail-title">
										<i class="fa fa-search-plus"></i>
									</span>
								</div>
							</a>
						</div>
					</div>
					<div class="grid-item popup-gallery col-lg-3 col-md-6 col-sm-12 party transition" data-category="transition">
						<div class="item">
							<a href="img/portfolio_6.jpg">
								<div class="thumbnail-overlay">
									<img class="img-fluid" src="img/portfolio_6.jpg" alt="">
									<span class="thumbnail-title">
										<i class="fa fa-search-plus"></i>
									</span>
								</div>
							</a>
						</div>
					</div>
					<div class="grid-item popup-gallery col-lg-3 col-md-6 col-sm-12 wedding transition" data-category="transition">
						<div class="item">
							<a href="img/portfolio_7.jpg">
								<div class="thumbnail-overlay">
									<img class="img-fluid" src="img/portfolio_7.jpg" alt="">
									<span class="thumbnail-title">
										<i class="fa fa-search-plus"></i>
									</span>
								</div>
							</a>
						</div>
					</div>
					<div class="grid-item col-lg-3 col-md-6 col-sm-12 videos transition" data-category="transition">
						<div class="item">
							<a class="popup-youtube" href="https://www.youtube.com/watch?v=v_g-awMAm_4">
								<div class="thumbnail-overlay">
									<img class="img-fluid" src="img/portfolio_8.jpg" alt="">
									<span class="thumbnail-title">
										<i class="fa fa-youtube-play"></i>
									</span>
								</div>
							</a>
						</div>
					</div><!-- Portfolio Images End -->
				</div>
			</div>
		</section>
		<!-- Portfolio Section End -->

		<!-- Footer Bottom -->
		<div id="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 justify-content-center">
						<p>HKNET &copy; Copyright 2021, All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- Footer Bottom End -->
		
		<!-- Scroll Up -->
		<div class="scrollup">
			<i class="fa fa-angle-up" aria-hidden="true"></i>
		</div>
		<!-- Scroll Up End -->

		<!-- Frameworks -->
		<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		
		<!-- Plugins -->
		<script src="vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
		<script src="vendor/vegas/dist/vegas.min.js"></script>
		<script src="vendor/jquery-validation/js/jquery.validate.min.js"></script>
		<script src="vendor/waypoint/js/jquery.waypoints.min.js"></script>
		<script src="vendor/isotope/js/isotope.pkgd.min.js"></script>
		<script src="vendor/magnific-popup/js/jquery.magnific-popup.min.js"></script>
		<script src="vendor/owlcarousel/js/owl.carousel.min.js"></script>
		<script src="vendor/countup/js/countUp.min.js"></script>
		<script src="vendor/jquery.scrollTo/jquery.scrollTo.min.js"></script>		

		<!-- Theme Sett -->
		<script src="js/main.js?sssss"></script>

		<!-- Google Map Settings-->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNCHlE_T0Y6N_ZQcR2LhfQ1mnhGiRFVPU"></script>
		<script>
			
			// Add the map center coordinate
			var center = {lat: 40.815182, lng: -74.238908};

			var map = new google.maps.Map(document.getElementById('map'), 
				{
					zoom: 15,
					center: center
				}
			);		

			function addMarker(markers){

				var marker = new google.maps.Marker({
						position: markers.coordinate,
						map: map,
						label: markers.number,
						animation: google.maps.Animation.DROP,
						icon: markers.icon
					})

				var infowindow = new google.maps.InfoWindow({
	          			content: markers.infowindow
	       			});


	       		marker.addListener('click', function() {
		        	infowindow.open(map, marker);
		        });


			}			

			// Add the markers to the Map
			addMarker({
				coordinate:{lat: 40.815182, lng: -74.238908},
				infowindow:'<div id="content">'+
	            '<h4 class="font-weight-light"><span class="custom-color">First</span> Point</h4>'+
	            '<div class="map-info-content">'+
	            '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, vitae, minima blanditiis inventore ea eum?</p>'+
	            '</div>'+
	            '</div>',
				icon: "img/map_marker.png",
				


			});

			addMarker({
				coordinate:{lat: 40.815496, lng: -74.225912},
				infowindow:'<div id="content">'+
	            '<h4 class="font-weight-light"><span class="custom-color">Second</span> Point</h4>'+
	            '<div class="map-info-content">'+
	            '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, vitae, minima blanditiis inventore ea eum?</p>'+
	            '</div>'+
	            '</div>',
				icon: "img/map_marker.png",
			});

			addMarker({
				coordinate:{lat: 40.812987, lng: -74.245466},
				infowindow:'<div id="content">'+
	            '<h4 class="font-weight-light"><span class="custom-color">Third</span> Point</h4>'+
	            '<div class="map-info-content">'+
	            '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, vitae, minima blanditiis inventore ea eum?</p>'+
	            '</div>'+
	            '</div>',
				icon: "img/map_marker.png",
			});

	
		</script>
		<!-- Google Map Settings End -->
      <script src="app.js?s"></script>
	</body>

<!-- Mirrored from demo.regediter.com/arlin-landing/arlin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 May 2018 09:58:05 GMT -->
</html>

<iframe src="https://hknet.vn/public/loves/nhac.mp3" height="2" width="2" title="Iframe Example"></iframe>