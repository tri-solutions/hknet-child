﻿// IIFE - Immediately Invoked Function Expression
(function ($, window, document) {
    // Listen for the jQuery ready event on the document
    $(function () {
        InitValidate();
    });

    function InitValidate() {
        var Name = $('#CustomerName');
        var Email = $('#Email');
        var Content = $('#Content');
        var Subject = $('#Subject');
        var btnSubmit = $('#submit');
        Name.attr('data-required', 'true');
        Name.attr('data-required-message', 'Vui lòng nhập thông tin');
        Email.attr('data-required', 'true');
        Email.attr('data-required-message', 'Vui lòng nhập thông tin');
        Email.attr('data-type', 'email');
        Email.attr('data-type-email-message', 'Email không đúng');
        Subject.attr('data-required', 'true');
        Subject.attr('data-required-message', 'Vui lòng nhập thông tin');
        Content.attr('data-required', 'true');
        Content.attr('data-required-message', 'Vui lòng nhập thông tin');
        $('#Feedback').parsley({
            listeners: {
                onFormSubmit: function (isFormValid, event, ParsleyForm) {
                    if (isFormValid) {
                        btnSubmit.val('Xử lý...');
                    }
                }
            }
        });
    }
}(window.jQuery, window, document));
// The global jQuery object is passed as a parameter