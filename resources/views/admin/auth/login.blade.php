<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
                <meta property="og:image" content="https://hknet.vn/admin/images/TIGVIET.png" />
                <meta property="og:description" content="Ưu đãi lên tới 1.500.000đ - Hỗ trợ data KH - Target nhiều tiêu chí" />
                <meta property="og:title" content="HKNet - Truyền Thông Mạng Xã Hội" />
    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>CÔNG TY TRUYỀN THÔNG HKNET - ĐĂNG NHẬP HỆ THỐNG</title>
    <link rel="shortcut icon" href="https://hknet.vn/hknet-logo.png">
    <meta property="fb:admins" content="100004185550959" />
    <meta name="description" />
    <meta name="keywords" />
    <meta name="author" />
    <meta property="og:title" content="Dịch vụ facebook, Google, Instagram, Youtube, Tư vấn hỗ trợ giải phải tiếp cận khách hàng" />
    <meta property="og:description" />
    <meta property="og:image" content="" />
    <meta property="og:image:secure_url" content="" />
    <meta property="og:image:width" content="" />
    <meta property="og:image:height" content="" />
    <meta property="og:type" content="Website" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta name="twitter:card" content="" />
    <meta name="twitter:title" content="Cung cấp dịch vụ trên nền tảng các mạng xã hội Facebook, Instagram, Youtube,... " />
    <meta name="twitter:description" />
    <meta name="twitter:image" content="" />
    <meta name="DC.language" content="scheme=utf-8 content=vi" />
    <link href="https://hknet.vn/" rel="canonical" />
    
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
	<link href="{{asset("admin/plugins/sweetalert/sweetalert.css")}}" rel="stylesheet" type="text/css" />
	<script src="{{asset("admin/plugins/jquery/jquery.min.js")}}"></script>
	<script src="{{asset("admin/js/custom.min.js")}}"></script>
	<script src="{{asset("admin/plugins/sweetalert/sweetalert.min.js")}}"></script>
	<script src="{{asset("admin/js/features/auth.js")}}"></script>




    <!-- Google Tag Manager -->
    <!-- End Google Tag Manager -->
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/css/smooth-scroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/jquery.lazy.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/owl.carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/Themes/js/isotope.min.js') }}"></script>

    

    <link media="screen" rel="stylesheet" type="text/css" href="{{ asset('/template2/Content/1/v2/css/bootstrap.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/h.css?loadsrssss') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/n.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/r.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/index.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/owl.carousel.css') }}" />
        <link media="screen" rel="stylesheet" type="text/css" href="https://hknet.vn/template2/Content/1/v2/css/cubeportfolieo.min.css" />



    <style>


        .client__item > a.btn-background {
            height: auto;
            border: none;
        }

        .bloghome__title {
            text-overflow: ellipsis;
            overflow: hidden;
            height: 1.2em;
            white-space: nowrap;
        }

        .client__body > p.btn-background {
            float: right;
        }

        .list-flex li {
            display: flex;
        }

        @media only screen and (max-width: 2400px) and (min-width: 1600px) {
            .footer:before {
                background-color: #F2F7FF;
                top: -332px;
            }
        }
    </style>

</head>

<body>

<section class="head">

<div class="head__two">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="head__two--inner">
<div class="head__two--logo">
<h1 title="">
<a href="/">
<img src="https://hknet.vn/admin/images/LOGO-hknet.png" align="logo">
</a>
</h1>
</div>

<div class="head__two--user">
<a id="aSignin" class="btn-outline" href="/login">Đăng Nhập</a>
<a id="aSignup" class="btn-background" href="/register">Đăng Ký</a>
<div class="btn-group" role="group" id="divAccount" style="display: none;">
<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border: 1px solid #7eb142;color: #7eb142;border-radius: 23px;">
<i class="fa fa-user-circle"></i> <span id="spanDisplayName"></span>
<span class="caret"></span>
</button>
</div>
</div>
<div class="head__two--nav-mobile">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" id="btn-menu-mobile">
<div class="sidebar-nav navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</div></button>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="why-choose row"style="margin-top: 70px;">
            <div class="container">
                <div class="container">
                    <div class="col-md-12">

                        <div class="cx">
                    
                            
                        <div class="col-lg-7"style="margin-top: 40px;>
                                    <div class="block-signin-text__block-text">
                                     <div class="block-signin-text__block-text-title">
                                      <h1><b>Dịch Vụ Facebook, Youtube, Tiktok, Instagram Giá Tốt Nhất</b></h1>
                                     </div>
                                     <div class="block-signin-text__block-text-description"><center><p>Website chính thức của Công Ty Truyền Thông HKNET ✔️</p></center></div>
                                <center><p><font color="green"></font></p><p></p><font color="green">
                                <img src="../admin/images/dichvu/facebook_vip.png" style="height:40px" alt="">
                                <img src="../admin/images/dichvu/instagram.png" style="height:40px" alt="">
                                <img src="../admin/images/dichvu/tiktok.png" style="height:40px" alt="">
                                <img src="../admin/images/dichvu/youtubeq.PNG" style="height:40px" alt="">
                                </font></center>
                        <div class="headline__intro">
                                                    <center><font color="red">Liên Hệ: 0812 8888 96 (Call/zalo)</font></center>
                        </div>

                                    </div>
                                   </div>
                            <div class="col-lg-5 blog-main">
                                <div class="why-choose__list">
                                	<div class="login-box card">
                        				<div class="card-body">
                        					<form class="form-horizontal form-material" id="loginForm">
                        					<center><div class="guide__detail"><h3>Đăng nhập</h3></div></center>
                        						<label for="username" class="control-label">Tên tài khoản</label>
                        						<div class="form-group pt-3">
                        							<div class="col-xs-12">
                        								<input class="form-control" type="text" name = "phone" required="" placeholder="Số Điện Thoại" /> </div>
                        						</div>
                        						  	<label for="password" class="control-label">Mật khẩu</label>

                        						<div class="form-group pt-3">
                        							<div class="col-xs-12">
                        								<input class="form-control" type="password" name = "password" required="" placeholder="Mật Khẩu" /> </div>
                        						</div>
                        						
                        						<div class="form-group text-center m-t-20">
                        							<div class="col-xs-12">
                        								<button class="btn btn-info btn btn-brand-02 btn-block" id = "loginBtn">Đăng Nhập</button>
                        								
                        							</div>
                        						</div>
                        						<a class="btn btn-info btn btn-brand-02 btn-block" href="/register">Đăng Ký</a>
                        					</form>
                        				</div>
                                </div>
                            </div>
                        </div>
                        
 
    
                        
                    </div>
                </div>
            </div>
            <br>
          <h6 class="text-center mt-4">HKNet là nhà cung cấp dịch vụ và phần mềm hỗ trợ tăng tương tác với khách hàng một cách nhanh nhất với chi phí tốt nhất.</h6>
        </section>
        
        


<section id="contact" class="center footers">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="footer__contact">
<div class="footer__logo2">
<img width="105" height="105" src="https://hknet.vn/ico.png" align="Logo">
</div>
<div class="footer__address">
<div class="footer__address--left">
<figure class="footer__address--image lazy" data-src="/template2/Content/1/v2/img/footer_address@2x.png"></figure>
</div>
<div class="footer__address--right">
<div class="footer__address--item">CÔNG TY TNHH CÔNG NGHỆ VÀ THƯƠNG MẠI QUỐC TẾ HK NET</div>
<div class="footer__address--item"> 113 Trần Văn Dư, Bắc Mỹ An, Ngũ Hành Sơn, Đà Nẵng 550000</div>
<a href="https://goo.gl/maps/Sveinox6eUnvQuL78" target="_blank" class="map-link">
<i class="fa fa-map-marker"></i>
<span> Open in Google Maps</span>
</a>
</div>
</div>
<div class="footer__phone">
<div class="footer__phone--left">
<figure class="footer__address--image lazy" data-src="/template2/Content/1/v2/img/footer_phone@2x.png"></figure>
</div>
<div class="footer__phone--right">
<div class="footer__address--item">SDT: <a href="tel:0812888896">0812.8888.96</a></div>
<div class="footer__address--item">Mail: <a href="mail:Support@hknet.vn">Support@hknet.vn</a></div>
</div>
</div>
<div class="footer__site">
<div class="footer__site--left">
<figure class="footer__address--image lazy" data-src="/template2/Content/1/v2/img/icon-www@2x.png"></figure>
</div>
<div class="footer__site--right">
<div data-id="27d1016" class="elementor-element elementor-element-27d1016 elementor-column elementor-col-25 elementor-top-column" data-element_type="column"><div class="elementor-column-wrap  elementor-element-populated"><div class="elementor-widget-wrap"><div data-id="3648821" class="elementor-element elementor-element-3648821 elementor-widget elementor-widget-heading" data-element_type="heading.default"><div class="elementor-widget-container"><h4 class="elementor-heading-title elementor-size-default">Kết nối chúng tôi</h4></div></div><div data-id="0318d71" class="elementor-element elementor-element-0318d71 elementor-widget elementor-widget-facebook-page" data-element_type="facebook-page.default"><div class="elementor-widget-container"><div class="elementor-facebook-widget fb-page fb_iframe_widget" data-href="https://www.facebook.com/hknet/" data-tabs="" data-height="100px" data-small-header="false" data-hide-cover="false" data-show-facepile="true" data-hide-cta="false" style="min-height: 1px;height:200px" data-width="290px" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=290&amp;height=100&amp;hide_cover=false&amp;hide_cta=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhknet%2F&amp;locale=vi_VN&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=&amp;width=290px"><span style="vertical-align: bottom; width: 290px; height: 130px;"><iframe name="f1e4e8477432e9c" height="100px" data-testid="fb:page Facebook Social Plugin" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v10.0/plugins/page.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df152834171d5f54%26domain%3Dhknet.vn%26origin%3Dhttps%253A%252F%252Fhknet.vn%252Ff2e61a13f980c7%26relation%3Dparent.parent&amp;container_width=290&amp;height=100&amp;hide_cover=false&amp;hide_cta=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhknet%2F&amp;locale=vi_VN&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=&amp;width=290px" style="border: none; visibility: visible; width: 290px; height: 130px;" class=""></iframe></span></div></div></div></div></div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
    



    

    <script type="text/javascript" src="{{ asset('/template2/Scripts/CustomValidate/eSMS.Uitl.js') }}" id="eSMSUtil') }}"></script>
    
        <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
            n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1273287776096843');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=1273287776096843&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->

    <!-- /JavaScripts -->
    <!-- Google Tag Manager (noscripts) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MNMZ896" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="alert alert-success" id="divAlert" style="position: fixed;z-index: 9999999;top: 100px;right: 0px; display:none;">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong id="alertTitle">Thất Bại</strong>
        <hr class="message-inner-separator" />
        <p id="alertContent">
            <strong>Success! </strong> Product have added to your wishlist.
        </p>
    </div>

    <script type="text/javascript">
        $(function () {
        try {
                window.eSMSSDK.Util.init(1, function (s) { console.log(s); })
            } catch (e) {
                console.log(e);
            }
        });

        function showAlert(id) {
            $("#" + id).fadeTo(2000, 500).slideUp(1000, function () {
                $("#" + id).slideUp(1000);
            });
        }

        function ShowNotifyAlert(message, typealert) {
            var popup = $("#divAlert");
            var title = $("#alertTitle");
            var content = $("#alertContent");

            if (typealert === 'info') {
                popup.removeClass('alert-danger');
                popup.addClass('alert-success');
                title.html('Thành công');
                content.html(message);
                showAlert('divAlert');
                return;
            }

            if (typealert === 'error') {
                popup.removeClass('alert-success');
                popup.addClass('alert-danger');
                title.html('Thất bại');
                content.html(message);
                showAlert('divAlert');
                return;
            }
        }
    </script>
    <script type="text/javascript" src="{{ asset('/template2/Scripts/feedback.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/scripts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/a.js') }}"></script>


    
        <!-- Messenger Plugin chat Code -->
    <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v10.0'
          });
        };

        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
      </script>

      <!-- Your Plugin chat code -->
      <div class="fb-customerchat"
        attribution="page_inbox"
        page_id="914109848730081">
      </div>
    

</body>
</html>