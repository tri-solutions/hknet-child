<!DOCTYPE html>
<html lang="en">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Favicon icon -->
	<link rel="icon" type="image/png" sizes="16x16" href="{{asset("admin/images/favicon.png")}}" />
	<title>HKNet - Đăng nhập</title>
	<!-- Bootstrap Core CSS -->
	<link href="{{asset("admin/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" />
	<!-- Custom CSS -->
	<link href="{{asset("admin/css/style.css")}}" rel="stylesheet" />
	<!-- You can change the theme colors from here -->
	<link href="{{asset("admin/css/colors/blue.css")}}"  id="theme" rel="stylesheet" />
	<link href="{{asset("admin/plugins/sweetalert/sweetalert.css")}}" rel="stylesheet" type="text/css" />

</head>

<body>
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinners.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<svg class="circular" viewbox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
	</div>
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<section id="wrapper">
		<div class="login-register" style="min-height: 100vh; background-image:url({{asset("admin/images/auth-bg.jpg")}});">
			<div class="login-box card">
				<div class="card-body">
					<form class="form-horizontal form-material" id="loginForm">
						<center><h3 class="box-title m-t-10 m-b-0 center"><b>Đăng Nhập</b></h3></center>
						<center><p><font color="green"></font></p><h5><font color="green"><b>Hệ thống cung cấp dịch vụ<br> MXH đa nền tảng</b></font></h5><p></p> 
						<img src="../admin/images/dichvu/facebook_vip.png" style="height:30px" alt=""></img>
						<img src="../admin/images/dichvu/instagram.png" style="height:30px" alt=""></img>
						<img src="../admin/images/dichvu/tiktok.png" style="height:30px" alt=""></img>
						<img src="../admin/images/dichvu/youtubeq.PNG" style="height:30px" alt=""></img>
						</center>
						
						<div class="form-group pt-3">
							<div class="col-xs-12">
								<input class="form-control" type="text" name = "phone" required="" placeholder="Số Điện Thoại" /> </div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<input class="form-control" type="password" name = "password" required="" placeholder="Mật Khẩu" /> </div>
						</div>
						<div class="form-group text-center m-t-20">
							<div class="col-xs-12">
								<button class="btn btn-info btn btn-brand-02 btn-block" id = "loginBtn">Đăng Nhập</button>
								
							</div>
						
						</div>
						
						<div class="col-sm-12 text-center"> Chưa có tài khoản? <a href="/register" class="text-info m-l-5"><b>Đăng Ký</b></a></div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- All Jquery -->
	<!-- ============================================================== -->
	<script src="{{asset("admin/plugins/jquery/jquery.min.js")}}"></script>
	<!-- Bootstrap tether Core JavaScript -->
	<script src="{{asset("admin/plugins/bootstrap/js/popper.min.js")}}"></script>
	<script src="{{asset("admin/plugins/bootstrap/js/bootstrap.min.js")}}"></script>
	<!-- slimscrollbar scrollbar JavaScript -->
	<script src="{{asset("admin/js/jquery.slimscroll.js")}}"></script>
	<!--Wave Effects -->
	<script src="{{asset("admin/js/waves.js")}}"></script>
	<!--Menu sidebar -->
	<script src="{{asset("admin/js/sidebarmenu.js")}}"></script>
	<!--stickey kit -->
	<script src="{{asset("admin/plugins/sticky-kit-master/dist/sticky-kit.min.js")}}"></script>
	<script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>
	<!--Custom JavaScript -->
	<script src="{{asset("admin/js/custom.min.js")}}"></script>
	<script src="{{asset("admin/plugins/sweetalert/sweetalert.min.js")}}"></script>

	<script src="{{asset("admin/js/features/auth.js")}}"></script>


	<!-- ============================================================== -->
	<!-- Style switcher -->
	<!-- ============================================================== -->
	<script src="{{asset("admin/plugins/styleswitcher/jQuery.style.switcher.js")}}"></script>
	
</body>

</html>