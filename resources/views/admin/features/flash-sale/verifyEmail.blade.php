@php
    $status = ['<span class="badge badge-warning">Đang Chạy</span>','<span class="badge badge-success">Đã hoàn thành</span>','<span class="badge badge-warning">Đang Chạy</span>', '<span class="badge badge-danger">Lỗi ID</span>'];
    $role = ['apiPrice','agencyPrice', 'collaboratorsPrice', 'guestPrice']

@endphp
@extends('admin.layouts.master')

@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Flash Sale Verify Email</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Chức năng</a></li>
                <li class="breadcrumb-item active">Verify Email</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-danger" role="alert">
                            <b>Các tính năng Flash Sale yêu cầu phải xác minh email </b>
                        </div>

                        <form class="form_ajax" action="/admin/mail/sendOtp">
                            <div class="row" style="align-items: center;">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="comment">Email:</label>
                                        <input class="form-control" name="email" type="email" required value="{!!$user->email!!}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-default">Send OTP</button>
                                </div>

                            </div>
                        </form>


                        <form class="form_ajax" action="/admin/mail/verify">
                            <div class="form-group">
                                <label for="comment">OTP:</label>
                                <input class="form-control" name="otp" type="text" required>
                            </div>

                            <button type="submit" class="btn btn-primary">Verify</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection
