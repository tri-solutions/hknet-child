@php
	$status = ['<span class="badge badge-warning">Đang Chạy</span>','<span class="badge badge-success">Đã hoàn thành</span>','<span class="badge badge-warning">Đang Chạy</span>', '<span class="badge badge-danger">Hoàn tiền</span>'];
    $role = ['apiPrice','agencyPrice', 'collaboratorsPrice', 'guestPrice'];
@endphp
@extends('admin.layouts.master')

@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">VIP Comment</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Chức năng</a></li>
				<li class="breadcrumb-item active">VIP Comment</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Tạo mới VIP Comment</h4>
						<h6 class="card-subtitle">Chức năng VIP Comment</h6>
						<div class="row pt-3">
							<div class="col-md-6 b-r">
								<form id = "infoForm">
									{{ csrf_field() }}
									<div class="form-group">
										<label>Link hoặc ID Page/Profile</label>
										<div class="input-group">
											<input type="text" class="form-control" id = "profileLink" placeholder="Nhập link hoặc ID Page/Profile">
											<div class="input-group-append">
												<button class="btn btn-info" id = "getProfileBtn" type="button">Lấy thông tin</button>
											</div>
										</div>
									</div>

									<div class = "profile-info">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<button class="btn btn-info" type="button">Profile Name</button>
											</div>
											<input name = "name" type="text" disabled class="form-control" aria-label="Username" aria-describedby="basic-addon1">
										</div>
										<div class="input-group">
											<div class="input-group-prepend">
												<button class="btn btn-info" type="button">Profile ID</button>
											</div>
											<input name ="id" type="text" disabled class="form-control" aria-label="Username" aria-describedby="basic-addon1">
										</div>
									</div>

									<div class = "order-info pt-3">

										<div class="row">

											<div class="col-12">
												<label>Giới tính: </label>
												<input class = "radio-col-light-blue" value = "Nam" name="gender" type="radio" id="radio_1">
												<label for="radio_1">Nam</label>
												<input class = "radio-col-light-blue" value = "Nữ" name="gender" type="radio" id="radio_2">
												<label for="radio_2">Nữ</label>
												<input class = "radio-col-light-blue" value = "Tất cả" name="gender" type="radio" id="radio_3" checked="">
												<label for="radio_3">Tất cả</label>

											</div>
											<div class="col-12">
												<div class="form-group">
													<label>Chọn gói VIP</label>
													<select data-price = "{{$service[$role[Auth::user()->role]]}}" class="custom-select" name = "package">
														<option value="5" selected>5 comments</option>
														<option value="10">10 comments</option>
														<option value="15">15 comments</option>
														<option value="20">20 comments</option>
														<option value="25">25 comments</option>
														<option value="30">30 comments</option>
														<option value="35">35 comments</option>
														<option value="40">40 comments</option>
														<option value="45">45 comments</option>
														<option value="50">50 comments</option>
													</select>
												</div>
											</div>

											<div class="col-12">
												<div class="form-group">
													<label>Thời hạn</label>
													<select class="custom-select col-12" name="period">
														<option value="30">1 tháng</option>
														<option value="60">2 tháng</option>
														<option value="90">3 tháng</option>

													</select>
												</div>
											</div>
											<div class="col-12">

												<div class="form-group">
													<label>Comments (mỗi dòng 1 comment)</label>
													<textarea data-price = "{{$service[$role[Auth::user()->role]]}}" name = "comment" class="form-control" rows="5"></textarea>
												</div>
											</div>

											<div class="col-12">

												<div class="form-group">
													<label>Ghi chú</label>
													<textarea name = "note" class="form-control" rows="5"></textarea>
												</div>
											</div>

										</div>


										<div class="row pt-3">
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Tổng tiền</button>
												</div>
												<input disabled data-price = "10000" value =0 name = "total" type="number" class="form-control">
											</div>
											<div class="col-md-6">
												<button type="button" data-service = "{{$service->id}}" data-function = "comment" id = "orderBtn" class="btn waves-effect waves-light btn-danger btn-block"><i class="fa fa-shopping-cart"></i> Thực hiện</button>
											</div>

										</div>

									</div>



								</form>
							</div>
                        	<div class="col-md-6">
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"> </button>
									<h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Lưu ý</h3>
									<p>+ Dịch vụ lên comment tự động khi đăng bài !</p>
									<p>+ Lượt comment tài khoản nuôi có thể trả lời comment</p>
								</div>
							</div>
						</div>
						<hr class = "py-3">
						<h4 class="card-title">Đơn hàng</h4>
						<h6 class="card-subtitle">Danh sách các đơn hàng của bạn</h6>
							<div class="table-responsive p-1">
								<table id ="myTable" class="table table-bordered">
									<thead>
										<tr>
											<th class = "text-center">#</th>
											<th>Dịch vụ</th>
											<th class = "text-center">Số lượng</th>
											<th>Facebook ID</th>
											<th class = "text-right">Tổng tiền</th>
											<th class = "text-right">Trạng thái</th>
											<th class = "text-right">Thông Báo</th>
											@if ($service->id == 16)
											<th class = "text-right">Ngày hết hạn</th>
											@endif
											<th class = "text-right">Thời gian</th>
                                            <th class = "text-right">Hành động</th>
                                        </tr>
									</thead>
									<tbody>
										@foreach ($orders as $order)
											<tr>
												<td></td>
												<td>{{$order->type->name}}</td>
												<td class = "text-center">{{$order->quantity}}</td>
												<td>{{$order->target}}</td>
												<td class = "text-right">{{$order->total}}</td>
												<td class = "text-right">{!!$status[$order->status]!!}</td>
												<td class="text-right"><span class="badge btn btn-outline-danger">{{$order->notify}}</span></td>
												@if ($service->id == 16)
												<td class = "text-right">
													{{date('d/m/Y', strtotime($order->created_at) + $order->duration)}}

                                                    <br />
                                                    @php
                                                        $start = strtotime('now');
                                                        $end = strtotime($order->created_at) + $order->duration;
                                                        $days_between = ceil(abs($end - $start) / 86400);
                                                        echo '( '. $days_between . ' day )';
                                                    @endphp
												</td>
												@endif
												<td class = "text-right">{{date('d/m/Y', strtotime($order->created_at))}}</td>
                                                <td class = "text-right">
                                                    <button type="button" class="btn btn-info btn-sm" data-action="edit" data-orderId="{{$order->id}}">Sửa</button>
                                                    <button type="button" class="btn btn-primary btn-sm" data-action="reOrder" data-orderId="{{$order->id}}">Gia hạn</button>
                                                    <button type="button" class="btn btn-danger btn-sm" data-action="delete" data-orderId="{{$order->id}}">Xoá</button>
                                                    @if($order->status === 1)
                                                    @endif
                                                </td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>

					</div>

				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
    <div class="modal" id="reOrderModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="reOrderForm">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Gia hạn thêm</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <input type="hidden" value="" id="reOrderId"/>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="col-12 mb-3">
                            <label>Thời hạn</label>
                            <select class="form-control" name="period" id="reOrderPeriod">
                                <option value=""> </option>
                                <option value="30"> 30 Ngày</option>
                                <option value="60"> 60 Ngày</option>
                                <option value="90"> 90 Ngày</option>
                            </select>

                        </div>
                    </div>

                    <hr />

                    <h3>Tổng tiền : <span id="totalReOrder"></span></h3>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary" style="float: left;">Gia hạn</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

        <div class="modal" id="editCommentModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="editCommentForm">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Edit Comments</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="col-12 mb-3">
                                <div class="form-group">
                                    <label>Comments (mỗi dòng 1 comment)</label>
                                    <textarea data-price = "{{$service[$role[Auth::user()->role]]}}" name = "comment" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary" style="float: left;">Cập nhật</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

@endsection

@section('scripts')
<script src="{{asset("admin/js/facebook-vip/comment.js?zxs")}}"></script>
@endsection