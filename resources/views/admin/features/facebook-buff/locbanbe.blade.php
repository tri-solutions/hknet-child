@php
	$status = ['<span class="badge badge-warning">Đang Chạy</span>','<span class="badge badge-success">Đã hoàn thành</span>','<span class="badge badge-warning">Đang Chạy</span>', '<span class="badge badge-danger">Hoàn tiền</span>'];
    $role = ['apiPrice','agencyPrice', 'collaboratorsPrice', 'guestPrice'];

@endphp
@extends('admin.layouts.master')

@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Chọc - Lọc Bạn Bè</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Chức năng</a></li>
				<li class="breadcrumb-item active">Chọc - Lọc Bạn Bè</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Thêm ID Lọc Bạn Bè</h4>
						<h6 class="card-Like Posttitle">Chức năng lọc bạn bè cho tài khoản facebook</h6>
						<div class="row pt-3">
							<div class="col-md-6 b-r">
								<form id = "infoForm">
									{{ csrf_field() }}
									<div class="form-group">
										<label>Facebook profile</label>
										<div class="input-group">
											<input type="text" class="form-control" id = "profileLink" placeholder="Nhập link hoặc profile id">
											<div class="input-group-append">
												<button class="btn btn-primary" id = "getProfileBtn" type="button">Lấy thông tin</button>
											</div>
										</div>
									</div>

									<div class = "profile-info">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<button class="btn btn-info" type="button">Profile Name</button>
											</div>
											<input name = "name" type="text" disabled class="form-control" aria-label="Username" aria-describedby="basic-addon1">
										</div>
										<div class="input-group">
											<div class="input-group-prepend">
												<button class="btn btn-info" type="button">Profile ID</button>
											</div>
											<input name ="id" type="text" disabled class="form-control" aria-label="Username" aria-describedby="basic-addon1">
										</div>
									</div>

									<div class = "order-info pt-3">

										<div class="row">
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Thời Hạn</button>
												</div>
                                                    <select class="form-control" name="reaction" id="reaction">
                                                                    <option value="30">1 Tháng</option>
                                                        </select>
                                                        </div>

										</div>

										<div class="row pt-3">
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Tổng tiền</button>
												</div>
												<input disabled data-price = "10000" value ="15000" name = "total" type="number" class="form-control">
											</div>
											<div class="col-md-6">
												<button type="button" data-service = "{{$service->id}}" data-function = "locbanbe" id = "orderBtn" class="btn waves-effect waves-light btn-danger btn-block"><i class="fa fa-shopping-cart"></i> Thực hiện</button>
											</div>

										</div>
									</div>

<p></p>

								</form>
							</div>
							<div class="col-md-6">
								<div class="alert alert-info">

                                        <div>
                                                        <p><span style="font-size:28px"><span style="color:#e74c3c">Tải Extension</span>:&nbsp;<a href="https://viplike.com.vn/AutoPoke.zip">Bấm vào đây để tải</a></span></p>
                                        <p>+ Chọc bạn bè ( nhằm tăng tương tác của tài khoản )</p>
                                        <p>+ Lọc bạn bè không tương tác</p>
                                        <p>*** Ưu điểm chỉ cần mua gói&nbsp;là bất cứ ai dowload extension đều được&nbsp; sử dụng UID đó .</p>
                                        <p>VD : UID A mua gói chọc lọc thì chỉ cần dowload extension về là dùng được không cần biết web là gì .&nbsp;</p>
                                        <p>Hướng dẫn cài đặt extension:</p>
                                        <p>Dowload và giải nén&nbsp; =&gt; ấn vào nút 3 chấm ở chomre ( ở góc trên cùng bên phải ) =&gt; chọn công cụ khác =&gt; tiền ích mở rộng</p>
                                        <p>=&gt; Bật chế độ cho nhà phát triển ( ở góc trên cùng bên phải ) =&gt; tải tiện ích đã giải nén rồi chọn extension&nbsp;</p>
                                        <p>*** Tránh checkpoint : vì sử dụng chính ip trên trình duyệt của bạn nên gần như cp khá ít .&nbsp;</p>
                                        <p>chọc thì chọc mỗi lúc 1 ít để thời gian cách ra&nbsp;</p>
                                        <p>lọc bạn bè thì xóa delay lớn hơn 3s cho an toàn</p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>

                                                    </div>

							</div>
						</div>
						<hr class = "py-3">
						<h4 class="card-title">Đơn hàng</h4>
						<h6 class="card-Like Posttitle">Danh sách các đơn hàng buff like Post của bạn</h6>
							<div class="table-responsive p-1">
								<table id ="myTable" class="table table-bordered">
									<thead>
										<tr>
											<th class = "text-center">#</th>
											<th>Dịch vụ</th>
											<th class = "text-center">Số lượng</th>
											<th>Facebook ID</th>
											<th class = "text-right">Tổng tiền</th>
											<th class = "text-right">Trạng thái</th>
											<th class = "text-right">Thời gian</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($orders as $order)
											<tr>
												<td></td>
												<td>{{$order->type->name}}</td>
												<td class = "text-center">{{$order->quantity}}</td>
												<td>{{$order->target}}</td>
												<td class = "text-right">{{$order->total}}</td>
												<td class = "text-right">{!!$status[$order->status]!!}</td>
												<td class = "text-right">{{$order->created_at}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>

					</div>

				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

@endsection

@section('scripts')
<script src="{{asset("admin/js/facebook-buff/sub.js")}}"></script>
@endsection