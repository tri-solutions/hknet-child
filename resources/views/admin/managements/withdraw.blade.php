@extends('admin.layouts.master')


@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Rút Tiền</h3>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row p-3">

                                <h3 class="card-title">Rút Tiền</h3>
                        </div>
                        <form id="rut_tien_vi_2" class="form_ajax" action="/admin/sendWithDrawRequest">
                            <div class="form-group">

                                <div class="form-group">
                                    <label for="">Số tài khoản:</label>
                                    <input class="form-control" type="text" name="stk" />
                                </div>

                                <div class="form-group">
                                    <label for="">Tên chủ tài khoản:</label>
                                    <input class="form-control" type="text" name="ctk" />
                                </div>

                                <div class="form-group">

							<label>Ngân Hàng / Ví Điện Tử:</label>
									<select  class="form-control" name="bank">
									<option value="Vietcombank">Vietcombank</option>
									<option value="Momo">Momo</option>
									<option value="AGB">Agribank</option>
									<option value="BAB">Bac A Bank</option>
									<option value="BIDV">BIDV</option>
									<option value="DAB">DongA Bank</option>
									<option value="EXB">Eximbank</option>
									<option value="GPB">GPBank</option>
									<option value="HDB">HDBank</option>
									<option value="MSB">MariTimeBank</option>
									<option value="MB">Military JSC Bank (MBB)</option>
									<option value="NAB">Nam A Bank</option>
									<option value="NVB">Nam Viet Bank (NaviBank)</option>
									<option value="OJB">OceanBank</option>
									<option value="PGB">PGBank</option>
									<option value="STB">Sacombank</option>
									<option value="SGB">SaiGon Bank for Industry and Trade (Saigon Bank)</option>
									<option value="SHB">SHB</option>
									<option value="TCB">Techcombank</option>
									<option value="TPB">TienPhong Bank</option>
									<option value="VIB">VIB</option>
									<option value="VAB">VietA Bank</option>
									<option value="ACB">ACB</option>
									<option value="ICB">VietinBank</option>
									<option value="VPB">VPBank</option>
									</select>

                                </div>
                                <div class="form-group">
                                    <label for="">Số tiền cần rút :</label>
                                    <input class="form-control" type="number" rows="5" name="amount" min="1000" />
(Tối đa <span class="text-warning font-weight-bold" id="coins">{{number_format(Auth::user()->balance_2)}}đ</span>)
                                </div>
                                <div style="display: flex; justify-content: space-between;">
                                    <button type="submit" class="btn btn-primary">Rút Tiền</button>
                                    <a class="btn btn-default" data-toggle="collapse" href="#withdrawHistory"
                                       role="button"
                                       aria-expanded="false" aria-controls="multiCollapseExample1">Lịch sử rút tiền</a>
                                </div>
                        </form>


                    </div>

                </div>
                </div>
                @php
                    use App\WithDrawRequests;use Illuminate\Support\Facades\Auth;$type = ['<span class="badge badge-danger">Lỗi</span>', '<span class="badge badge-success">Thành Công</span>', '<span class="badge badge-warning">Lệnh Đang Xử Lý</span>'];
                    $user = Auth::user();
                    $withDrawRequestModel = new WithDrawRequests();
                    $withDrawRequests = $withDrawRequestModel->get($user->id)
                @endphp
                <div class="collapse multi-collapse show" id="withdrawHistory">
                    <div class="card card-body">
                        <div class="table-responsive p-1">
                                <h3 class="card-title">Lịch Sử Rút Tiền</h3>
                            <table id="myTable" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Banking Info</th>
                                    <th>Balance</th>
                                    <th>Status</th>
                                    <th>Note</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($withDrawRequests as $request)
                                    <tr>
                                        <td>{{$loop->index + 1}}</td>
                                        <td>
                                            {!!$request->bank_info!!}
                                        </td>

                                        <td>
                                            {!!$request->balance!!}
                                        </td>

                                        <td>
                                            {!!$type[$request->status]!!}
                                        </td>

                                        <td>
                                            {!!$request->note!!}
                                        </td>
                                        <td>
                                            {{date('H:i:s d/m/Y', strtotime($request->created_at))}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset("admin/js/features/user.js")}}"></script>


@endsection
