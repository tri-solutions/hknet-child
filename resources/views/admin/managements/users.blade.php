@php
    $role = ['Admin','Đại lý', 'CTV', 'Khách'];
@endphp
@extends('admin.layouts.master')

@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Users Management</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
                <li class="breadcrumb-item active">Users</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row p-3">
                            <div class="col-md-8">
                                <h3 class="card-title">Users List</h3>
                            </div>
                            <div class="col-md-4">


                            </div>

                        </div>
                        <div class="table-responsive p-1">
                            <table id="myTable" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Name</th>
                                    <th class="text-right">Phone</th>
                                    <th class="text-right">RefLink</th>
                                    <th class="text-right">Cấp bậc</th>
                                    <th class="text-right">Số dư</th>
                                    <th class="text-right">Action</th>
                                    <th class="text-right">Created At</th>
                                    <th class="text-right">Update At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td class="text-center">{{$loop->index + 1}}</td>
                                        <td>
                                            {{$user->name}}
                                        </td>
                                        <td class="text-right">
                                            {{$user->phone}}
                                        </td>
                                        <td class="text-right">
                                            {{$user->refer_user}}
                                        </td>
                                        <td class="text-right">
                                            {{$role[$user->role]}}
                                        </td>
                                        <td class="text-right">
                                            {{$user->balance}}
                                        </td>
                                        <td class="text-right product-action">
                                            <a href="#" data-id={{$user->id}} class = "mr-3 addMoneyBtn" data-toggle="modal" data-target="#addMoneyModal"> <i class="fa fa-money"></i> </a>
                                            <a href="" data-id={{$user->id}} class = "mr-3 changeRoleBtn" data-toggle="modal" data-target="#changeRoleModal"> <i class="fa fa fa-vcard"></i> </a>
                                            <a href="" data-id={{$user->id}} class = "deleteBtn" data-toggle="tooltip" data-original-title="Xoá"> <i class="fa fa-close text-danger"></i> </a>
                                        </td>
                                        <td class="text-right">
                                            {{date('H:i:s d/m/Y', strtotime($user->created_at))}}
                                        </td>
                                        <td class="text-right">
                                            {{date('H:i:s d/m/Y', strtotime($user->updated_at))}}
                                        </td>


                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12">
            @php
                $user = \Illuminate\Support\Facades\Auth::user();
                $withDrawRequestModel = new \App\WithDrawRequests();
                $withDrawRequests = $withDrawRequestModel->get();
                $requestTypes = ['<span class="badge badge-danger">Lỗi</span>', '<span class="badge badge-success">Thành công</span>', '<span class="badge badge-warning">Đang Duyệt</span>'];
            @endphp
            <div class="card card-body">
                <h3 class="card-title">Rút tiền ví 2</h3>

                <div class="table-responsive p-1">
                    <table class="myTable table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Banking Info</th>
                            <th>Balance</th>
                            <th>Status</th>
                            <th>Note</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($withDrawRequests as $request)
                            <tr>
                                <td>{{$loop->index + 1}}</td>
                                <td>
                                    {!!$request->bank_info!!}
                                </td>

                                <td>
                                    {!!number_format($request->balance)!!}
                                </td>

                                <td>
                                    {!!$requestTypes[$request->status]!!}
                                </td>

                                <td>
                                    {!!$request->note!!}
                                </td>
                                <td>
                                    {{date('H:i:s d/m/Y', strtotime($request->created_at))}}
                                </td>

                                <td style="min-width: 10vw;">
                                    <a href="#withDrawActions{{!!$request->id!!}}}" class="btn btn-default"
                                       data-toggle="collapse">Actions</a>
                                    <div id="withDrawActions{{!!$request->id!!}}}" class="collapse">
                                        <form class="form_ajax" action="/admin/withDraw/update">

                                            <input type="hidden" name="id" value="{!!$request->id!!}" />
                                            <div class="form-group">
                                                <label for="sel1">Status:</label>
                                                <select class="form-control" name="status">
                                                    <option value="0" {{$request->status === 0 ? "selected" : ""}}>
                                                        Fail
                                                    </option>
                                                    <option value="1" {{$request->status === 1 ? "selected" : ""}}>
                                                        Done
                                                    </option>
                                                    <option value="2" {{$request->status === 2 ? "selected" : ""}}>
                                                        Wait
                                                    </option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="comment">Note:</label>
                                                <textarea class="form-control" rows="2" name="note"
                                                          id="comment">{!!$request->note!!}</textarea>
                                            </div>

                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </form>
                                    </div>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
        </div>

        <div class="modal fade" id="addMoneyModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
            <div class="modal-dialog" role="document">
                <div class="preloader">
                    <svg class="circular" viewbox="25 25 50 50">
                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2"
                                stroke-miterlimit="10"></circle>
                    </svg>
                </div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Nạp tiền</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form class="form-material p-1" id="addMoneyForm">
                            <div class="form-group">
                                <label for="name" class="control-label">Số tiền: </label>
                                <input type="text" placeholder="VD: 20000" class="form-control" name="amount" />
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="addMoneyBtn" class="btn btn-primary">Nạp tiền</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="changeRoleModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
            <div class="modal-dialog" role="document">
                <div class="preloader">
                    <svg class="circular" viewbox="25 25 50 50">
                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2"
                                stroke-miterlimit="10"></circle>
                    </svg>
                </div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Cài đặt tài khoản</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">

                        <form id="changeRole_form">

                            <div class="form-group">
                                <label for="role" class="control-label">Role:</label>

                                <select name="role" class="form-control" id="user-role">
                                    <option value="1">Đại lý</option>
                                    <option value="2">CTV</option>
                                    <option value="3">Khách</option>
                                    </option></select>
                            </div>


                            <div class="form-group">
                                <label for="pwd">Đặt lại mật khẩu:</label>
                                <input type="password" class="form-control" id="pwd" name="newPassword">
                            </div>

                            <input type="checkbox" id="accept_money_transfer" name="accept_money_transfer">
                            <label for="accept_money_transfer">Cho phép chuyển tiền</label><br>




                        {{--                    <div class="custom-control custom-checkbox" style="padding-left: 0;">--}}
                        {{--                        <input type="checkbox" class="custom-control-input" id="accept_money_transfer" name="accept_money_transfer">--}}
                        {{--                        <label class="custom-control-label" for="accept_money_transfer">Cho phép chuyển tiền</label>--}}
                        {{--                    </div>--}}

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="changeRoleBtn" class="btn btn-primary">Lưu</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->

        {{-- Khanh Du  --}}

        @endsection

        @section('scripts')
            <script src="{{asset("admin/js/features/user.js")}}"></script>


@endsection
